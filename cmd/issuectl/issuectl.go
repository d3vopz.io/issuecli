/*
Copyright 2021
*/

package main

import (
	"os"

	cmd "d3vopz.io/issuelib/pkg/cmd"
)

func main() {
	cmd.FactoryApplyGet()
	os.Exit(0)

}
