package main

import (
	"os"

	//cmd "d3vopz.io/issuelib/pkg/cmd"

	cmd "d3vopz.io/issuelib/pkg/cmd"

	kong "github.com/alecthomas/kong"
	log "github.com/sirupsen/logrus"
)

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.TraceLevel)
}

type ApplyCmd struct {
	File string `arg name:"file" help:"file to apply." type:"file"`
}

var cli struct {
	Apply ApplyCmd `cmd help:"Apply file."`
}

func (l *ApplyCmd) Run() error {

	log.Info("Applying '" + l.File + "'.")

	factoryApply, err := cmd.FactoryApplyGet(l.File)

	if factoryApply == nil {
		return err
	}

	return err
}

func main() {
	log.Info("Starting application")

	ctx := kong.Parse(&cli)
	switch ctx.Command() {
	case "apply <file>":
		log.Debug("Running command '" + ctx.Command() + "'")
		err := ctx.Run()
		ctx.FatalIfErrorf(err)
		break
	default:
		log.Error(ctx.Command())
	}
	log.Info("Exit application")
}

/**
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Launch",
            "type": "go",
            "request": "launch",
            "mode": "auto",
            "program": "${fileDirname}",
            "env": {},
            "args": ["apply", "testfile.yml"]
        }
    ]
}
*/
