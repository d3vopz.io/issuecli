module d3vopz.io/issuecli

go 1.16


require (
    d3vopz.io/issuelib v0.0.0
    github.com/alecthomas/kong v0.2.16
    github.com/sirupsen/logrus v1.8.1

)

replace (
     d3vopz.io/issuelib v0.0.0 => "../../d3vopz.io/issuelib"
)